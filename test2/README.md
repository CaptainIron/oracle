# 实验2：用户及权限管理

- 学号：202010414222，姓名：杨锦丰，班级：2班

## 实验目的

- 掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色cdu，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有cdu的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户yjf，给用户分配表空间，设置限额为50M，授予cdu角色。
- 最后测试：用新用户yjf连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

- 第1步：以system登录到pdborcl，创建角色cdu和用户yjf，并授权和分配空间：
```sql
$ sqlplus system/123@pdborcl
CREATE ROLE cdu;
GRANT connect,resource,CREATE VIEW TO cdu;
CREATE USER yjf IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER yjf default TABLESPACE "USERS";
ALTER USER yjf QUOTA 50M ON users;
GRANT cdu TO yjf;
--收回角色
REVOKE cdu FROM yjf;
```
> 语句“ALTER USER yjf QUOTA 50M ON users;”是指授权yjf用户访问users表空间，空间限额是50M。

![](f1.png)

- 第2步：新用户yjf连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。
```sql
$ sqlplus yjf/123@pdborcl
SQL> show user;
USER is "yjf"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
NAME
--------------------------------------------------
zhang
wang
```
![](f2.png)
![](f3.png)

- 第3步：用户hr连接到pdborcl，查询yjf授予它的视图customers_view
```sql
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM yjf.customers;
elect * from yjf.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> SELECT * FROM yjf.customers_view;
NAME
--------------------------------------------------
zhang
wang
```
![](f4.png)

## 概要文件设置,用户最多登录时最多只能错误3次
```sql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```
![](f5.png)
- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user yjf unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl
SQL> alter user yjf  account unlock;
```

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色cdu和用户yjf。
> 新用户yjf使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```
![](f6.png)
- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。
## 实验结束删除用户和角色
```sh
$ sqlplus system/123@pdborcl
SQL>
drop role cdu;
drop user yjf cascade;
```
![](f7.png)
## 结论
- 本次实验让我们掌握了如何创建用户与角色，以及如何为用户授权，掌握了用户之间共享对象的操作技能，以及概要文件对用户的限制。以下是Oracle用户及权限管理的一些重要方面：
- 用户创建和授权 : 创建用户是指将新用户添加到数据库中。Oracle数据库管理员可以使用“CREATE USER”命令创建新用户。在创建用户后，数据库管理员必须对用户进行授权，以指定该用户可以访问哪些数据库对象和执行哪些操作。授权可以使用“GRANT”命令完成。
- 角色管理 :Oracle数据库管理员可以创建角色并将权限分配给角色。用户可以被授予角色，从而具有该角色的所有权限。当需要更改权限时，只需更改角色权限，而无需更改每个用户的权限。可以使用“CREATE ROLE”命令来创建新角色，使用“GRANT”命令将权限分配给角色，并使用“GRANT”命令将角色授权给用户。
- 权限等级 :Oracle数据库中有不同的权限等级，例如系统级权限和对象级权限。系统级权限可能允许数据库管理员执行操作，如备份和恢复数据库，而对象级权限可能是对特定表格或视图执行的操作。授权时需要特别注意权限等级。
- 用户锁定和解锁 :数据库管理员可以锁定用户帐户以防止其他人访问其数据或更改其对象。这可以通过使用“ALTER USER”命令完成。解锁用户帐户时，管理员可以使用“ALTER USER”命令将其重新启用。
- Oracle用户及权限管理是Oracle数据库管理员需要熟练掌握的关键技能。正确地创建用户、运用角色、管理权限等级以及锁定解锁用户帐户等操作，可以确保数据的安全性和完整性。

