 # 实验5：包，过程，函数的用法
##    姓名: 杨锦丰    学号:202010414222
## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

- 代码
```sql
create or replace PACKAGE MyPack IS
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```
![img.png](img1.png)
## 测试
- 函数Get_SalaryAmount()测试方法：
```sh
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

输出：
DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
           10 Administration                 4848
           20 Marketing                      19896
           30 Purchasing                     27588
           40 Human Resources                6948
           50 Shipping                       176560

过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/
```
![img.png](img2.png)
## 总结
- 本次实验我们主要掌握了包，过程，函数的用法，它们都用于存储和管理 SQL 代码，提供代码的封装和重用。
- 包是一种逻辑上组合在一起的过程和函数的集合，以及共享变量、常量和游标等资源。包提供了一种有组织的方式来存储和管理相关的过程和函数，使代码更容易维护和重用。使用包可以提高代码的可读性、可维护性和执行效率。包中的过程和函数可以通过包名和过程名或函数名进行调用。
- 过程是一种存储在数据库中的可重用代码块，可以在应用程序或其他存储过程的上下文中进行调用。过程可以执行一系列 SQL 语句和 PL/SQL 代码块，它们可以接受输入参数和输出参数来进行交互。过程通常用于执行可复用的一组 SQL 语句或执行一些特定的数据操作。
- 函数与过程类似，也是存储在数据库中的可重用代码块，但函数返回一个值作为结果。函数可以用于执行复杂的计算或转换，它们可以接受输入参数并返回一个单一值。函数通常用于计算特定的值或转换数据类型。
- 在使用这些存储过程时，可以使用 CREATE PROCEDURE/ FUNCTION/ PACKAGE 命令来创建它们，然后使用 CALL/EXECUTE/SELECT FROM 包名过程名/函数名 来调用它们。
- 需要注意的是存储过程应该设计为尽可能短小精悍，以提高性能和可维护性；同时使用过程和函数的时候也要注意数据类型的匹配，确保输入输出参数的正确性。