# 基于Oracle数据库的商品销售系统的设计

- 学号：202010414222，姓名：杨锦丰，班级：2班


## 实验内容

设计一套基于Oracle数据库的商品销售系统的数据库设计方案。

- 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
- 设计权限及用户分配方案。至少两个用户。
- 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
- 设计一套数据库的备份方案。
## 实验步骤

### 1.登录system用户，创建SALES_DATA，PRODUCT_DATA两个表空间以及ORDER,SALES_RECORD,CUSTOMER,PRODUCT四张表，并向其中插入数据:
```sql
CREATE TABLESPACE sales_data
DATAFILE '/home/oracle/app/oracle/oradata/orcl/pdborcl/sales_data01.dbf'
SIZE 100M AUTOEXTEND ON NEXT 100M;

CREATE TABLESPACE product_data
DATAFILE '/home/oracle/app/oracle/oradata/orcl/pdborcl/product_data01.dbf'
SIZE 100M AUTOEXTEND ON NEXT 100M;
```
![img.png](img1.png)
```sql
CREATE TABLE customer_orders (
  order_id NUMBER PRIMARY KEY,
  customer_id NUMBER,
  product_id NUMBER,
  quantity NUMBER,
  order_date DATE,
  payment_status VARCHAR2(20),
  CONSTRAINT fk_customer
    FOREIGN KEY (customer_id)
    REFERENCES customer(customer_id),
  CONSTRAINT fk_product
    FOREIGN KEY (product_id)
    REFERENCES product(product_id)
)
TABLESPACE sales_data;

CREATE TABLE sales_record (
  sale_id NUMBER PRIMARY KEY,
  order_id NUMBER,
  sales_volume NUMBER,
  sales_amount NUMBER,
  sale_date DATE,
  CONSTRAINT fk_order
    FOREIGN KEY (order_id)
    REFERENCES customer_orders(order_id)
)
TABLESPACE sales_data;

CREATE TABLE customer (
  customer_id NUMBER PRIMARY KEY,
  customer_name VARCHAR2(50),
  contact_info VARCHAR2(50),
  address VARCHAR2(100)
)
TABLESPACE sales_data;

CREATE TABLE product (
  product_id NUMBER PRIMARY KEY,
  product_name VARCHAR2(50),
  price NUMBER,
  description VARCHAR2(100)
)
TABLESPACE product_data;

```
![img.png](img2.png)
![img.png](img3.png)

- 插入数据
```sql
-- 向customer表中插入数据
DECLARE
  name_prefix VARCHAR2(10) := 'Customer_';
BEGIN
  FOR i IN 1..10000 LOOP
    INSERT INTO customer (customer_id, customer_name, contact_info, address)
    VALUES (i, name_prefix || i, 'contact_' || i || '@example.com', 'Address_' || i);
  END LOOP;
  COMMIT;
END;

-- 向product表中插入数据
DECLARE
  name_prefix VARCHAR2(10) := 'Product_';
BEGIN
  FOR i IN 1..10000 LOOP
    INSERT INTO product (product_id, product_name, price, description)
    VALUES (i, name_prefix || i, 10.0 * i, 'Description for product ' || i);
  END LOOP;
  COMMIT;
END;

-- 向customer_orders表中插入数据
DECLARE
  orders_per_customer NUMBER := 10;
BEGIN
  FOR i IN 1..10000 LOOP
    FOR j IN 1..orders_per_customer LOOP
      INSERT INTO customer_orders (order_id, customer_id, product_id, quantity, order_date, payment_status)
      VALUES ((i - 1) * orders_per_customer + j, i, j, 1, SYSDATE - j, 'PAID');
    END LOOP;
  END LOOP;
  COMMIT;
END;

-- 向sales_record表中插入数据
DECLARE
  sales_per_order NUMBER := 5;
BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO sales_record (sale_id, order_id, sales_volume, sales_amount, sale_date)
    SELECT ROW_NUMBER() OVER (ORDER BY dbms_random.value) id,
           order_id,
           dbms_random.value(1, 5),
           dbms_random.value(5, 50),
           SYSDATE - dbms_random.value(1, 10)
    FROM customer_orders SAMPLE(1) WHERE ROWNUM = 1;
    IF MOD(i, sales_per_order) = 0 THEN
      UPDATE customer_orders SET payment_status = 'PAID' WHERE order_id = (i / sales_per_order) * sales_per_order;
    END IF;
  END LOOP;
  COMMIT;
END;
/
```
### 2.创建用户和角色，分配适当权限并授予他们对表空间和表的访问权限。
- 创建用户：
```sql
CREATE USER sales_user IDENTIFIED BY password DEFAULT TABLESPACE sales_data;
CREATE USER product_user IDENTIFIED BY password DEFAULT TABLESPACE product_data;
```
- 创建角色：
```sql
CREATE ROLE sales_role;
CREATE ROLE product_role;
```
- 将用户分配给角色：
```sql
GRANT sales_role TO sales_user;
GRANT product_role TO product_user;
```
- 授予角色适当的权限：
```sql
GRANT CONNECT, RESOURCE, CREATE SESSION TO sales_role;
GRANT CONNECT, RESOURCE, CREATE SESSION TO product_role;
- 将表空间分配给角色：
```sql
GRANT UNLIMITED TABLESPACE TO sales_user;
GRANT UNLIMITED TABLESPACE TO product_user;
```
- 为每个表授予适当的权限：
```sql
GRANT SELECT, INSERT, UPDATE, DELETE ON customer_orders TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON sales_record TO sales_role;
GRANT SELECT ON customer TO sales_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON product TO product_role;
```
![img.png](img4.png)
### 3.在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑:
- 计算销售记录中每个客户的总销售额
- 在订单被插入时，自动更新客户的最近购买日期
- 在商品被插入、更新或删除时，自动更新与之关联的订单和销售记录
```sql
CREATE OR REPLACE PACKAGE sales_package AS
  /* 计算销售记录中每个客户的总销售额 */
  PROCEDURE calculate_sales(customer_id IN CUSTOMER.customer_id%TYPE);
  /* 在订单被插入时，自动更新客户的最近购买日期 */
  PROCEDURE update_customer_latest_purchase(order_id IN ORDER.order_id%TYPE);
  /* 在商品被插入、更新或删除时，自动更新与之关联的订单和销售记录 */
  PROCEDURE update_related_sales(product_id IN PRODUCT.product_id%TYPE,
                                 operation IN VARCHAR2);
END sales_package;
/
```
-   计算销售记录中每个客户的总销售额 
```
CREATE OR REPLACE PACKAGE BODY sales_package AS
  PROCEDURE calculate_sales(customer_id IN CUSTOMER.customer_id%TYPE) IS
    total_sales NUMBER := 0;
  BEGIN
    SELECT SUM(sales_amount)
    INTO total_sales
    FROM sales_record
    WHERE order_id IN (
      SELECT order_id
      FROM order
      WHERE customer_id = customer_id
    );
    -- 更新客户表中的总销售额字段
    UPDATE customer
    SET total_sales = total_sales + NVL(customer.total_sales, 0)
    WHERE customer_id = customer_id;
  END calculate_sales;
  /
 ```
-  在订单被插入时，自动更新客户的最近购买日期 
```
  PROCEDURE update_customer_latest_purchase(order_id IN ORDER.order_id%TYPE) IS
    purchase_date DATE;
  BEGIN
    SELECT order_date
    INTO purchase_date
    FROM order
    WHERE order_id = order_id;
    UPDATE customer
    SET latest_purchase = purchase_date
    WHERE customer_id = (
      SELECT customer_id
      FROM order
      WHERE order_id = order_id
    );
  END update_customer_latest_purchase;
  /
```
- 在商品被插入、更新或删除时，自动更新与之关联的订单和销售记录 
```
  PROCEDURE update_related_sales(product_id IN PRODUCT.product_id%TYPE,
                                 operation IN VARCHAR2) IS
  BEGIN
    IF operation = 'INSERT' THEN
      -- 如果是插入操作，仅需要更新销售记录中的商品描述字段
      UPDATE sales_record
      SET product_description = (
        SELECT description
        FROM product
        WHERE product_id = product_id
      )
      WHERE order_id IN (
        SELECT order_id
        FROM order
        WHERE product_id = product_id
      );
    ELSE
      -- 如果是更新或删除操作，需要先获取商品的详细信息
      DECLARE
        product_name VARCHAR2(50);
        price NUMBER;
        description VARCHAR2(100);
      BEGIN
        SELECT product_name, price, description
        INTO product_name, price, description
        FROM product
        WHERE product_id = product_id;
        IF operation = 'UPDATE' THEN
          -- 如果是更新操作，需要同时更新订单和销售记录相关的字段
          UPDATE order
          SET product_name = product_name,
              price = price
          WHERE product_id = product_id;
          UPDATE sales_record
          SET product_name = product_name,
              sales_amount = sales_volume * price,
              product_description = description
          WHERE order_id IN (
            SELECT order_id
            FROM order
            WHERE product_id = product_id
          );
        ELSE
          -- 如果是删除操作，需要删除与之相关的订单和销售记录
          DELETE FROM order
          WHERE product_id = product_id;
          DELETE FROM sales_record
          WHERE order_id NOT IN (
            SELECT order_id
            FROM order
          );
        END IF;
      END;
    END IF;
  END update_related_sales;
END sales_package;
/
```
- 在上面的代码中，我们定义了一个名为 sales_package 的程序包，其中包含了三个存储过程，用于实现我们所需的业务逻辑。具体来说：
- calculate_sales 存储过程用于计算指定客户的总销售额，然后将结果更新到客户表中的总销售额字段。
- update_customer_latest_purchase 存储过程用于在订单被插入时，自动更新客户表中的最近购买日期字段。
- update_related_sales 存储过程用于在商品被插入、更新或删除时，自动更新与之关联的订单和销售记录。具体来说，如果是插入操作，则只需要更新销售记录中的商品描述字段；如果是更新操作，则需要同时更新订单和销售记录相关的字段；如果是删除操作，则需要删除与之相关的订单和销售记录。
       通过将这些存储过程封装在程序包中，我们可以更加方便地管理和调用它们，从而实现复杂的业务逻辑。


### 4.设计一套数据库的备份方案
- 设计一套数据库的备份方案，使用RMAN进行数据的备份与恢复：
```sql
$ sqlplus / as sysdba
SQL> SHUTDOWN IMMEDIATE;
SQL> STARTUP MOUNT;
SQL> ALTER DATABASE ARCHIVELOG;
SQL> ALTER DATABASE OPEN;

```
![img.png](img5.png)
```sql
$ rman target /
RMAN> SHOW ALL;
RMAN> BACKUP DATABASE;
RMAN> LIST BACKUP;

```
![img.png](img6.png)
## 总结
- 表设计及表空间设计：需要了解Oracle数据库的表结构、各种数据类型、表空间的概念、创建表空间语法等知识点。
- 数据库用户及权限分配设计：需要了解Oracle数据库的用户及权限管理，包括创建用户、授权、撤销权限等操作，以及用户的角色与权限的关系等知识点。
- 程序包设计及使用PL/SQL语言：需要了解Oracle数据库中的PL/SQL语言，包括存储过程、函数、触发器、游标、异常处理、动态SQL语句等知识点，同时还需要了解如何建立程序包以组织和管理代码。
- RMAN备份设计：需要了解Oracle数据库备份的基本概念、备份策略的设计、RMAN的基本操作、备份的恢复和验证等知识点。
- 总的来说，设计一个基于Oracle数据库的商品销售系统需要涉及多方面的知识，包括数据库基础、SQL语言、PL/SQL编程、备份与恢复等方面的知识点。同时还需要对商品销售系统的业务流程和实际运作有一定的了解和理解。
